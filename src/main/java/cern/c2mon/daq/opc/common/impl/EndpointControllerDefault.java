/*******************************************************************************
 * Copyright (C) 2010-2016 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cern.c2mon.daq.opc.common.impl;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import cern.c2mon.daq.common.IEquipmentMessageSender;
import cern.c2mon.daq.opc.AbstractEndpointController;
import cern.c2mon.daq.opc.common.IOPCEndpointFactory;
import cern.c2mon.shared.common.process.IEquipmentConfiguration;

/**
 * EndpointControllerDefault class
 *
 * @author vilches
 */
@Slf4j
public class EndpointControllerDefault extends AbstractEndpointController {

    /**
     * Creates a new default EndpointController
     *
     * @param endPointFactory
     *            The endpoint factory to create OPC endpoints.
     * @param sender
     *            The equipment message sender to send updates to.
     * @param factory
     *            Factory to crate equipmen bound loggers.
     * @param opcAddresses
     *            The default addresses for the endpoints.
     * @param equipmentConfiguration
     *            The equipment configuration for this controller.
     */
    public EndpointControllerDefault(final IOPCEndpointFactory endPointFactory, final IEquipmentMessageSender sender,
        final List<OPCDefaultAddress> opcAddresses, final IEquipmentConfiguration equipmentConfiguration) {
        this.opcEndpointFactory = endPointFactory;
        this.sender = sender;
        this.opcAddresses = opcAddresses;
        this.equipmentConfiguration = equipmentConfiguration;
    }
}
